# Dotfiles

Managed with GNU stow.

# Setup

```shell
git clone $HOME/.dotfiles
cd $HOME/.dotfiles
stow <software>
```