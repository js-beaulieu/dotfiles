/*
 * ROFI One Dark
 *
 * Based on OneDark.vim (https://github.com/joshdick/onedark.vim)
 *
 * Author: Benjamin Stauss
 * User: me-benni
 *
 */


* {
  black:      #000000;
  red:        #eb6e67;
  green:      #95ee8f;
  yellow:     #f8c456;
  blue:       #6eaafb;
  mangenta:   #d886f3;
  cyan:       #6cdcf7;
  emphasis:   #50536b;
  text:       #dfdfdf;
  text-alt:   #b2b2b2;
  fg:         #abb2bf;
  bg:         #282c34cc;
  darker:     #0F131B;

  spacing: 0;
  background-color: transparent;

  font: "Roboto 10";
  text-color: @text;
}

window {
  fullscreen: true;
  transparency: "true";
  background-color: #00000000;
}

mainbox {
  margin: 38% 42%;
  padding: 10px;
  background-color: @bg;
  border: 2px;
  border-color: @darker;
}

inputbar {
  margin: 0px 0px 20px 0px;
  children: [prompt, textbox-prompt-colon, entry, case-indicator];
}

prompt {
  text-color: @red;
}

textbox-prompt-colon {
  expand: false;
  str: ":";
  text-color: @text-alt;
}

entry {
  margin: 0px 5px;
}

listview {
  spacing: 2px;
  dynamic: true;
  scrollbar: false;
}

element {
  padding: 5px;
  text-color: @text-alt;
  highlight: bold #eb6e67; /* red */
}

element selected {
  background-color: @darker;
  text-color: @fg;
}

element urgent, element selected urgent {
  text-color: @red;
}

element active, element selected active {
  text-color: @purple;
}

message {
  padding: 5px;
  background-color: @emphasis;
  border: 1px;
  border-color: @cyan;
}

button selected {
  padding: 5px;
  background-color: @emphasis;
}


