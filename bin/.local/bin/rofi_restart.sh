#!/usr/bin/env bash

function gen_choices()
{
    echo "lock"
    echo "exit"
    echo "restart"
    echo "shutdown"
}

CHOICE=$( gen_choices | rofi -theme onedark -monitor primary -dmenu -p "power management")

case "$CHOICE" in
lock)
    slock
;;
exit)
    i3-msg exit
;;
restart)
    systemctl reboot
;;
shutdown)
    systemctl poweroff -i
;;
*)
    exit
;;
esac
