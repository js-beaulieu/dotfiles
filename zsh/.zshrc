# Dependencies
# TODO - Have an actual way to setup dependencies on first install. Ansible playbook, basic script?
# - fzf
# - sdkman : https://sdkman.io/
# - android-studio : install https://www.jetbrains.com/toolbox-app/, then install sdk w/ defaults
# - ripgrep
# - antibody : https://getantibody.github.io/
# - bat
# - exa

########################################
# Environment variables
########################################
# fzf
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'

# sdkman
export SDKMAN_DIR="$HOME/.sdkman"

# history
export HISTFILE=~/.zsh_history
export HISTSIZE=20000
export SAVEHIST=20000

# android
export ANDROID_HOME="$HOME/Android/Sdk"
export ANDROID_SDK_ROOT="$HOME/Android/Sdk"

# path
typeset -U path
path=($HOME/.bin $HOME/.local/bin $path)
[ -d "$HOME/.yarn/bin" ] && path=($HOME/.yarn/bin $HOME/.config/yarn/global/node_modules/.bin $path)
[ -d "$HOME/.poetry/bin" ] && path=($HOME/.poetry/bin $path)
[ -d "$HOME/.pyenv" ] && path=($HOME/.pyenv/bin $path)
[ -d "$HOME/tools/flutter/bin" ] && path=($HOME/tools/flutter/bin $path)
[ -d "$HOME/Android/Sdk" ] && path=($HOME/Android/Sdk/emulator $HOME/Android/Sdk/platform-tools $HOME/Android/Sdk/tools $HOME/Android/Sdk/tools/bin $path)
[ -d "$HOME/.cargo/bin" ] && path=($HOME/.cargo/bin $path)
export PATH

########################################
# Misc
########################################
# shell
setopt auto_cd

# load ssh keys
privatekeys=()
for publickey in ${HOME}/.ssh/*.pub; do
    privatekeys+=(`basename ${publickey%.*}`)
done
zstyle :omz:plugins:ssh-agent identities $privatekeys

# pyenv
if [ -x "$(command -v pyenv)" ]; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# direnv
eval "$(direnv hook zsh)"

# antibody
autoload -Uz compinit
compinit -i
[ -f $HOME/.zsh_plugins.sh ] && . $HOME/.zsh_plugins.sh

# SdkMan
[[ -s "/home/jsbeaulieu/.sdkman/bin/sdkman-init.sh" ]] && source "/home/jsbeaulieu/.sdkman/bin/sdkman-init.sh"

# fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

########################################
# Aliases / functions
########################################
# aliases
alias ls="exa --color=always"
alias ll="ls -lh"
alias la="ls -lha"
alias zshr="antibody bundle < ~/.zsh_plugins.txt > $HOME/.zsh_plugins.sh && echo 'static plugin file generated'"
alias tna="tmux new -A -s"
alias da="python django-admin.py"
alias mp="python manage.py"
alias vim="nvim"
alias cat="bat --theme=TwoDark"

# functions
function mcd() {
    mkdir -p $@ && cd ${@:$#}
}

function dockstop() {
    if [ -z "$1" ]; then
        echo "Stopping all containers"
        docker stop $(docker ps -q)
    else
        echo "Stopping containers starting with '[$1]'"
        docker ps --format '{{.Names}}' | grep "$1" | awk '{print $1}' | xargs -I {} docker stop {}
    fi
}

# Machine specific configuration
[ -f $HOME/.zsh_local.zsh ] && . $HOME/.zsh_local.zsh
